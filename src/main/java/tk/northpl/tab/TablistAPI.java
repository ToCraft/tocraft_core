package tk.northpl.tab;

import org.bukkit.entity.Player;
import pl.tocraft.core.CorePlugin;

@SuppressWarnings("unused")
public final class TablistAPI {
    private TablistAPI() {
    }

    public static void setTabSlot(final int x, final int y, final String content) {
        CorePlugin.getTabListHandler().getSlot(x, y).setGlobalText(content);
    }

    public static void setTabSlot(final Player p, final int x, final int y, final String content) {
        CorePlugin.getTabListHandler().getSlot(x, y).setTextForPlayer(p.getName(), content);
    }

    public static void updateSkin(final int x, final int y, String newNick) {
        if (newNick == null) {
            newNick = "MHF_OakLog";
        }
        CorePlugin.getTabListHandler().updateSkin(x, y, newNick);
    }

    public static void updateSkin(final Player p, final int x, final int y, String newSkin) {
        if (newSkin == null) {
            newSkin = "MHF_OakLog";
        }
        CorePlugin.getTabListHandler().getSlot(x, y).setSkinForPlayer(p.getName(), newSkin);
    }
}
