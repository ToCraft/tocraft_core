package pl.tocraft.core.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.tocraft.core.CorePlugin;

public class WarpCommand extends Command {

    public WarpCommand() {
        super(new String[]{"warp"});
        super.setDescription("Lista warpow, teleportacja na warpa");
        super.setUsage("[warp]");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        if (args.length != 0) {
            if (!(sender instanceof Player)) {
                throw new CommandException();
            }
            Player p = (Player) sender;
            CorePlugin.teleportToWarp(p, args[0].toLowerCase());
        } else {
            CorePlugin.listWarps(sender);
        }
    }

}
