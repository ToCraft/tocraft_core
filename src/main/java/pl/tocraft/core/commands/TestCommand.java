package pl.tocraft.core.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.tocraft.core.CorePlayer;
import pl.tocraft.core.CorePlugin;

public class TestCommand extends Command {

    public TestCommand() {
        super(new String[]{"test"});
        setDescription("Test.");
        setPermission("testcommand");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        Player player = (Player)sender;
        CorePlayer cPlayer = CorePlayer.getCorePlayer(player.getUniqueId());
        CorePlugin.sendPrefixed(player, "Tier: " + cPlayer.getDropTier());
        CorePlugin.sendPrefixed(player, "Exp: " + cPlayer.getDropExp());
        CorePlugin.sendPrefixed(player, "UUID: " + cPlayer.getUUID());
    }

}
