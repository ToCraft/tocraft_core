package pl.tocraft.core.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.themolka.cmds.command.Commands;
import pl.themolka.cmds.command.UsageException;
import pl.tocraft.core.CorePlugin;

import java.util.List;
import java.util.stream.Collectors;

public class HelpCommand extends Command {
    private static final int COMMANDS_PER_PAGE = 10;

    public HelpCommand() {
        super(new String[]{"help", "pomoc", "?", "h"});
        setUsage("[strona]");
        setDescription("Wyswietla pomoc");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        final List<Command> list = Commands.getCommands().stream()
                .filter(command -> !command.hasPermission() || sender.hasPermission(command.getPermission())) //If hasn't permission don't list it
                .collect(Collectors.toList());

        int page;
        try {
            page = args.length == 0 ? 1 : Integer.parseInt(args[0]);
            if (page < 1) //Cannot be negative
                throw new UsageException(null);
        } catch (NumberFormatException e) {
            throw new UsageException(null);
        }

        if (page > Math.ceil((float) list.size() / (float) COMMANDS_PER_PAGE))
            throw new CommandException(CorePlugin.getTag() + ChatColor.RED + "Zbyt duzy numer strony!");

        sender.sendMessage(CorePlugin.getTag() + ChatColor.WHITE + "Pomoc" + (page != 1 ? (ChatColor.AQUA + " - Strona " + page) : "") + ChatColor.WHITE + ":");

        for (int i = 0; i < 10; i++) {
            if (list.size() <= i + (page - 1) * COMMANDS_PER_PAGE)
                return;
            Command command = list.get(i + (page - 1) * COMMANDS_PER_PAGE);
            sender.sendMessage(ChatColor.GRAY + "/" + command.getName() + ChatColor.DARK_AQUA + " - " + ChatColor.GOLD + command.getDescription());
        }
        if (page != list.size() / COMMANDS_PER_PAGE - 1) //Not last page
            //todo: buggy!
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aUzyj komendy&b /help " + (page + 1) + "&a aby zobaczyc wiecej komend!"));
    }
}
