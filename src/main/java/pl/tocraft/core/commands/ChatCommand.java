package pl.tocraft.core.commands;

import org.bukkit.command.CommandSender;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.tocraft.core.CorePlugin;

public class ChatCommand extends Command {

    public ChatCommand() {
        super(new String[]{"chat", "lockchat", "togglechat"});
        super.setDescription("Zablokuj/odblokuj czat");
        super.setPermission("tocraft.core.chat");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        CorePlugin.toogleChat();
    }

}
