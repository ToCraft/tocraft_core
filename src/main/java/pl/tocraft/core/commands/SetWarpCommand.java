package pl.tocraft.core.commands;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.themolka.cmds.command.UsageException;
import pl.tocraft.core.CorePlugin;

public class SetWarpCommand extends Command {

    public SetWarpCommand() {
        super(new String[]{"setwarp"});
        super.setDescription("Ustawia warp");
        super.setUsage("<warp>");
        super.setPermission("tocraft.core.setwarp");
    }

    @Override
    public void handle(Player player, String label, String[] args) throws CommandException {
        if (args.length != 0) {
            Location l = player.getLocation();
            CorePlugin.createWarp(l.getX(), l.getY(), l.getZ(), l.getYaw(), l.getWorld().getName(), l.getPitch(), args[0].toLowerCase(), player);
        } else {
            throw new UsageException(null);
        }
    }

}
