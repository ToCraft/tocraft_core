package pl.tocraft.core.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.tocraft.core.CorePlugin;
import pl.tocraft.core.utils.ExpUtil;

public class SellExpCommand extends Command {

    public SellExpCommand() {
        super(new String[]{"sellexp"});
        super.setDescription("Sprzedaj punkty doswiadczenia.");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        Player player = (Player) sender;
        if (args.length == 0) {
            player.sendMessage(CorePlugin.getTag() + ChatColor.WHITE + "Masz " + ExpUtil.getTotalExperience(player) + " punktow doswiadczenia, mozesz je sprzedac za 0.1TC/szt.");
            player.sendMessage("Aby sprzedac exp wpisz /sellexp [ilosc]");
        } else {
            if (Integer.parseInt(args[0]) > ExpUtil.getTotalExperience(player)) {
                player.sendMessage(CorePlugin.getTag() + ChatColor.WHITE + "Nie masz tylu punktow doswiadzenia.");
                return;
            } else {
                ExpUtil.setTotalExperience(player, ExpUtil.getTotalExperience(player) - Integer.parseInt(args[0]));
                player.sendMessage(CorePlugin.getTag() + ChatColor.WHITE + "Sprzedales " + Integer.parseInt(args[0]) + " expa za " + Integer.parseInt(args[0]) * 0.1 + "TC");
                CorePlugin.economy.depositPlayer(player, Integer.parseInt(args[0]) * 0.1);
            }
        }
    }

}
