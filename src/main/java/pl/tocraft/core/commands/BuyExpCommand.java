package pl.tocraft.core.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.tocraft.core.CorePlugin;
import pl.tocraft.core.utils.ExpUtil;

public class BuyExpCommand extends Command {

    public BuyExpCommand() {
        super(new String[]{"buyexp"});
        setDescription("Kup punkty doswiadczenia.");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        Player player = (Player) sender;
        if (args.length == 0) {
            player.sendMessage(CorePlugin.getTag() + ChatColor.WHITE + "Cena jednego punktu doswiadczenia wynosi 0.2TC");
            player.sendMessage("Aby kupic exp wpisz /buyexp [ilosc]");
        } else {
            if (Integer.parseInt(args[0]) * 0.2 > CorePlugin.economy.getBalance(player)) {
                player.sendMessage(CorePlugin.getTag() + ChatColor.WHITE + "Nie posiadasz tylu pieniedzy.");
            } else {
                ExpUtil.setTotalExperience(player, ExpUtil.getTotalExperience(player) + Integer.parseInt(args[0]));
                player.sendMessage(CorePlugin.getTag() + ChatColor.WHITE + "Kupiles " + Integer.parseInt(args[0]) + " expa za " + Integer.parseInt(args[0]) * 0.2 + "TC");
                CorePlugin.economy.withdrawPlayer(player, Integer.parseInt(args[0]) * 0.2);
            }
        }
    }

}
