package pl.tocraft.core.commands;

import org.bukkit.command.CommandSender;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.themolka.cmds.command.UsageException;
import pl.tocraft.core.CorePlugin;

public class DelWarpCommand extends Command {

    public DelWarpCommand() {
        super(new String[]{"delwarp"});
        super.setDescription("Usun warp");
        super.setUsage("<warp>");
        super.setPermission("tocraft.core.delwarp");

    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        if (args.length != 0) {
            CorePlugin.delWarp(args[0].toLowerCase(), sender);
            return;
        }
        throw new UsageException("Podaj nazwe warpu!");
    }

}
