package pl.tocraft.core.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.tocraft.core.CorePlayer;
import pl.tocraft.core.CorePlugin;

public class DropCommand extends Command {

    public DropCommand() {
        super(new String[]{"drop"});
        super.setDescription("Informacje na temat dropu ze stone");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        CorePlayer player = CorePlayer.getCorePlayer(((Player) sender).getUniqueId());
        if(player.getDropTier() != 5){ //wyswietlaj te informacje tylko na 1-4
            CorePlugin.sendPrefixed(sender, "Drop jest oparty na \"tierach\", aby zdobyc nastepny \"tier\" nalezy zdobyc dana ilosc punktow doswiadczenia.");
            CorePlugin.send(sender, "Twoj tier to &6" + player.getDropTier());
            CorePlugin.send(sender, "Posiadasz " + player.getDropExp() + "/" + expToLevel(player.getDropTier()) + " punktow doswiadczenia.");
        }
        CorePlugin.send(sender, "&cDrop: &f(przedmioty wypadaja z kamienia)");
        if(player.getDropTier() == 5){
            CorePlugin.send(sender, "&a[Tier 5] &fDiament: " + CorePlugin.getDropChance("diamond") + "%");
        }else{
            CorePlugin.send(sender, "&c[Tier 5] &fDiament: " + CorePlugin.getDropChance("diamond") + "%");
        }
        if(player.getDropTier() >= 4){
            CorePlugin.send(sender, "&a[Tier 4] &fSzmaragd: " + CorePlugin.getDropChance("emerald") + "%");
        }else{
            CorePlugin.send(sender, "&c[Tier 4] &fSzmaragd: " + CorePlugin.getDropChance("emerald") + "%");
        }
        if(player.getDropTier() >= 3){
            CorePlugin.send(sender, "&a[Tier 3] &fRedstone: " + CorePlugin.getDropChance("redstone") + "%");
        }else{
            CorePlugin.send(sender, "&c[Tier 3] &fRedstone: " + CorePlugin.getDropChance("redstone") + "%");
        }
        if(player.getDropTier() >= 2){
            CorePlugin.send(sender, "&a[Tier 2] &fZloto: " + CorePlugin.getDropChance("gold") + "%");
            CorePlugin.send(sender, "&a[Tier 2] &fLapis: " + CorePlugin.getDropChance("lapis") + "%");
        }else{
            CorePlugin.send(sender, "&c[Tier 2] &fZloto: " + CorePlugin.getDropChance("gold") + "%");
            CorePlugin.send(sender, "&c[Tier 2] &fLapis: " + CorePlugin.getDropChance("lapis") + "%");
        }
        CorePlugin.send(sender, "&a[Tier 1] &fZelazo: " + CorePlugin.getDropChance("iron") + "%");
        CorePlugin.send(sender, "&a[Tier 1] &fWegiel: " + CorePlugin.getDropChance("coal") + "%");
    }

    private int expToLevel(int level){
        switch(level){
            case 1:
                return 1000;
            case 2:
                return 4000;
            case 3:
                return 15000;
            case 4:
                return 30000;
            default:
                return 0;
        }
    }

}
