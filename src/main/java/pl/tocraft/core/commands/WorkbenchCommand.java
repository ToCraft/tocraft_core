package pl.tocraft.core.commands;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.tocraft.core.CorePlugin;

public class WorkbenchCommand extends Command {
    public WorkbenchCommand() {
        super(new String[]{"workbench", "crafting"});
        super.setDescription("Mobilny crafting table");
        setPermission("tocraft.core.workbench");
    }

    @Override
    public void handle(Player player, String label, String[] args) throws CommandException {
        player.openWorkbench(null, true);
    }
}
