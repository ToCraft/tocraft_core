package pl.tocraft.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.themolka.cmds.command.UsageException;
import pl.tocraft.core.CorePlugin;
import pl.tocraft.core.McPlayer;

public class VanishCommand extends Command {

    public VanishCommand() {
        super(new String[]{"v", "vanish"});
        super.setDescription("Pojawiaj sie i znikaj");
        super.setPermission("tocraft.core.vanish");
        super.setUsage("[gracz]");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        if (args.length == 1 && !sender.hasPermission("mcwyspa.core.vanish.others"))
            throw new CommandException(CorePlugin.getTag() + ChatColor.RED + "Nie mozesz uzywac vanisha na innych graczach!");

        if (args.length == 0 && !(sender instanceof Player))
            throw new UsageException(CorePlugin.getTag() + ChatColor.RED + "Musisz podac nick gracza!");

        McPlayer target = args.length == 0 ? CorePlugin.getMcPlayer((Player) sender) : CorePlugin.getMcPlayer(Bukkit.getPlayer(args[0]));

        if (target == null)
            throw new CommandException(CorePlugin.getTag() + ChatColor.RED + "Nie znaleziono gracza: " + args[0] + "!");

        target.setVanished(!target.isVanished());

        if (target.isVanished()) {
            target.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 0));
            CorePlugin.playerForEachFiltered(p -> !p.getPlayer().hasPermission("mcwyspa.core.vanish"), p -> p.getPlayer().hidePlayer(target.getPlayer()));
        } else {
            target.getPlayer().removePotionEffect(PotionEffectType.INVISIBILITY);
            CorePlugin.playerForEach(p -> p.getPlayer().showPlayer(target.getPlayer()));
        }
        target.sendPrefixed("&aJestes teraz &b" + (target.isVanished() ? "niewidzialny" : "znow widzialny") + "!");

        if (target.getPlayer() != sender)
            CorePlugin.sendPrefixed(sender, "&aGracz&b " + target.getPlayer() + "&a jest teraz &b" + (target.isVanished() ? "niewidzialny" : "znow widzialny") + "!");
    }
}
