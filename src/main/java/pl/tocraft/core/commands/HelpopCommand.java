package pl.tocraft.core.commands;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.themolka.cmds.command.UsageException;
import pl.tocraft.core.CorePlugin;

public class HelpopCommand extends Command {
    public HelpopCommand() {
        super(new String[]{"helpop"});
        super.setDescription("Popros administratora o pomoc.");
        setUsage("[wiadomosc]");
    }

    @Override
    public void handle(Player player, String label, String[] args) throws CommandException {
        if(args.length == 0){
            throw new UsageException(null);
        }
        StringBuilder message = new StringBuilder();
        for (int i = 0; i < args.length; i++)
            message.append(args[i]).append(" ");

        boolean isAdminOnline = false;

        for(Player p : Bukkit.getOnlinePlayers()){
            if(p.hasPermission("tocraft.core.helpop.recive")){
                p.sendMessage(ChatColor.DARK_AQUA + "[HelpOp] " + ChatColor.WHITE + player.getName() + ": " + message);
                isAdminOnline = true;
            }
        }
        if(isAdminOnline){
            CorePlugin.sendPrefixed(player, "Wiadomosc zostala wyslana. Poczekaj cierpliwie na odpowiedz i nie spamuj.");
        }else{
            CorePlugin.sendPrefixed(player, "Brak administratorow online, sproboj za pare minut.");
        }
    }
}
