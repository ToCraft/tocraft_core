package pl.tocraft.core.commands;

import org.bukkit.entity.Player;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.tocraft.core.CorePlugin;

public class SetSpawnCommand extends Command {
    public SetSpawnCommand() {
        super(new String[]{"setspawn"});
        super.setDescription("Ustaw spawn");
        super.setPermission("tocraft.core.setspawn");
    }

    @Override
    public void handle(Player player, String label, String[] args) throws CommandException {
        CorePlugin.setSpawnLocation(player.getWorld().getName(), player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ(), player.getLocation().getYaw(), player.getLocation().getPitch());
        CorePlugin.sendPrefixed(player, "Spawn zostal ustawiony.");
    }
}
