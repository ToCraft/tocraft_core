package pl.tocraft.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.themolka.cmds.command.UsageException;
import pl.tocraft.core.CorePlugin;
import pl.tocraft.core.McPlayer;

public class KickCommand extends Command {

    public KickCommand() {
        super(new String[]{"kick"});
        super.setDescription("Wyrzuc gracza z serwera");
        super.setPermission("tocraft.core.kick");
        super.setUsage("<gracz> <powod>");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        if (args.length < 2)
            throw new UsageException(null);

        McPlayer target = CorePlugin.getMcPlayer(Bukkit.getPlayer(args[0]));

        if (target == null)
            throw new CommandException(CorePlugin.getTag() + ChatColor.RED + "Nie znaleziono gracza: " + args[0] + "!");


        StringBuilder reasonBuilder = new StringBuilder();

        for (int i = 1; i < args.length; i++)
            reasonBuilder.append(args[i]).append(" ");

        final String reason = reasonBuilder.toString().trim();

        target.getPlayer().kickPlayer(CorePlugin.getTag() + "\n" +
                        ChatColor.RED + "Zostales wyrzucony z serwera!\n" +
                        ChatColor.GREEN + "Powod: " + ChatColor.YELLOW + reason + "\n" +
                        ChatColor.GREEN + "Przez: " + ChatColor.YELLOW + sender.getName()
        );

        CorePlugin.playerForEachFiltered(p -> p.getPlayer().hasPermission("tocraft.core.kick"), p -> p.sendPrefixed("&b" + sender.getName() + "&a wyrzucil gracza &b" + target.getName() + "&a za &b" + reason + "!"));
    }
}
