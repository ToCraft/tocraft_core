package pl.tocraft.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.themolka.cmds.command.UsageException;
import pl.tocraft.core.CorePlugin;
import pl.tocraft.core.McPlayer;

public class FlyCommand extends Command {

    public FlyCommand() {
        super(new String[]{"fly"});
        super.setDescription("Latanie");
        super.setPermission("tocraft.core.fly");
        super.setUsage("[nick]");
    }

    @Override
    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        if (args.length == 1 && !sender.hasPermission("mcwyspa.core.fly.others"))
            throw new CommandException(CorePlugin.getTag() + ChatColor.RED + "Nie mozesz uzywac fly na innych graczach!");
        if (args.length == 0 && !(sender instanceof Player)) //Console
            throw new UsageException(CorePlugin.getTag() + ChatColor.RED + "Musisz podac nick gracza!");

        McPlayer target = args.length == 0 ? CorePlugin.getMcPlayer((Player) sender) : CorePlugin.getMcPlayer(Bukkit.getPlayer(args[0]));

        if (target == null)
            throw new CommandException(CorePlugin.getTag() + ChatColor.RED + "Nie znaleziono gracza: " + args[0] + "!");

        boolean fly = !target.getPlayer().getAllowFlight();

        target.getPlayer().setAllowFlight(fly);
        target.getPlayer().setFlying(fly);

        target.sendPrefixed("&aLatanie: " + (fly ? "&2wlaczone" : "&cwylaczone") + "!");

        if (target.getPlayer() != sender)
            target.sendPrefixed("&aLatanie: " + (fly ? "&2wlaczone" : "&cwylaczone") + "&a dla &b" + target.getName() + "&a!");
    }
}
