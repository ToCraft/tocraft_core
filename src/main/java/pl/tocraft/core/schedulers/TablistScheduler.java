package pl.tocraft.core.schedulers;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import pl.tocraft.core.CorePlugin;
import tk.northpl.tab.TablistAPI;

/**
 * Created by artur on 12.08.15.
 */
public class TablistScheduler {
    CorePlugin plugin;

    public TablistScheduler(CorePlugin plugin) {
        this.plugin = plugin;
        Bukkit.getScheduler().runTaskTimer(plugin, () -> {
            // Clear tablist
            int cSlot = 0;
            int x = 0;
            int y = 2;
            while (cSlot <= 71) {
                if (x == 4) {
                    y++;
                    x = 0;
                }
                TablistAPI.setTabSlot(x, y, "");
                TablistAPI.updateSkin(x, y, null);
                x++;
                cSlot++;
            }
            // Fill tablist with online players
            int pSlot = 0;
            int px = 0;
            int py = 2;
            for (Player p : Bukkit.getOnlinePlayers()) {
                if (pSlot <= 71) {
                    if (px == 4) {
                        py++;
                        px = 0;
                    }

                    TablistAPI.setTabSlot(px, py, p.getName()); //set player name
                    TablistAPI.updateSkin(px, py, p.getName()); //set player skin
                    px++;
                    pSlot++;
                }
            }
        }, 20L, 40L);
    }
}
