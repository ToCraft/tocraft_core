package pl.tocraft.core.recipes;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;

public class NetherStarCrafting
{
    public NetherStarCrafting(){
        ItemStack netherStar = new ItemStack(Material.NETHER_STAR, 1);
        ShapedRecipe recipe = new ShapedRecipe(netherStar);
        recipe.shape(" # ", "#$#", " # ");
        recipe.setIngredient('#', Material.OBSIDIAN);
        recipe.setIngredient('$', Material.DIAMOND_BLOCK);
        Bukkit.getServer().addRecipe(recipe);
    }
}
