package pl.tocraft.core.recipes;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

/**
 * Created by artur on 19.08.15.
 */
public class TreasureChestCrafting {

    public TreasureChestCrafting(){
        ItemStack item = new ItemStack(Material.CHEST, 1);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(ChatColor.GOLD + "Skrzynia Skarbow");

        ArrayList<String> lore = new ArrayList<>();
        lore.add(ChatColor.GREEN + "Kliknij PPM aby otworzyc.");
        meta.setLore(lore);

        meta.addEnchant(Enchantment.LOOT_BONUS_BLOCKS, 10, true);
        item.setItemMeta(meta);

        ShapedRecipe recipe = new ShapedRecipe(item);
        recipe.shape("###", "#$#", "###");
        recipe.setIngredient('#', Material.DIAMOND);
        recipe.setIngredient('$', Material.CHEST);
        Bukkit.getServer().addRecipe(recipe);
    }
}
