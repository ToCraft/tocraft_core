package pl.tocraft.core;

import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.*;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.*;
import org.bukkit.plugin.java.JavaPlugin;
import pl.themolka.cmds.command.Commands;
import pl.tocraft.core.commands.*;
import pl.tocraft.core.economy.EconomyAPI;
import pl.tocraft.core.economy.SLAPI;
import pl.tocraft.core.economy.VaultConnector;
import pl.tocraft.core.economy.commands.EcoCommand;
import pl.tocraft.core.economy.commands.MoneyCommand;
import pl.tocraft.core.economy.commands.PayCommand;
import pl.tocraft.core.economy.schedulers.EconomySaveScheduler;
import pl.tocraft.core.listeners.*;
import pl.tocraft.core.recipes.*;
import pl.tocraft.core.schedulers.TablistScheduler;
import tk.northpl.tab.TabListHandler;
import tk.northpl.tab.TablistAPI;

import java.io.IOException;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class CorePlugin extends JavaPlugin {

    private static final HashMap<Player, McPlayer> playersCache = new HashMap<>();
    public static Permission permission = null;
    public static Economy economy = null;
    public static Chat chat = null;
    public static TabListHandler tablistHandler;
    protected static CorePlugin plugin;
    public Random random;
    public List<Material> durabilityFixAppliedItems = new ArrayList<>();
    public List<Material> durabilityFixSwords = new ArrayList<>();
    public boolean chatLocked;

    public void onEnable() {
        //Plugin
        plugin = this;
        chatLocked = false;

        //Register configs
        ConfigManager.registerConfig("spawn", "spawn.yml", this);
        ConfigManager.registerConfig("drop", "drop.yml", this);
        ConfigManager.registerConfig("warps", "warps.yml", this);
        ConfigManager.registerConfig("balance", "balance.yml", this);
        ConfigManager.loadAll();
        ConfigManager.saveAll();

        //Vault
        registerEconomy(); //Register ToCraft Economy :D
        setupChat(); //Chat support
        setupEconomy(); //Economy support?
        setupPermissions(); //Permissions support


        //Register listeners
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new PlayerJoinListener(), this);
        pm.registerEvents(new PlayerQuitListener(), this);
        pm.registerEvents(new DropListener(), this);
        pm.registerEvents(new SignListener(), this);
        pm.registerEvents(new ChatListener(), this);
        pm.registerEvents(new PlayerDeathListener(), this);
        pm.registerEvents(new TablistPlayerJoinListener(), this);
        pm.registerEvents(new TablistPlayerLeaveListener(), this);
        pm.registerEvents(new EntityDeathListener(), this);
        pm.registerEvents(new ServerPingListener(), this);
        pm.registerEvents(new WeatherChangeListener(), this);
        pm.registerEvents(new PlayerPortalListener(), this);
        pm.registerEvents(new TreasureChestListener(), this);

        //Register commands
        Commands.register(this, HelpCommand.class);
        Commands.register(this, SpawnCommand.class);
        Commands.register(this, SetSpawnCommand.class);
        Commands.register(this, WhoCommand.class);
        Commands.register(this, GamemodeCommand.class);
        Commands.register(this, WarpCommand.class);
        Commands.register(this, SetWarpCommand.class);
        Commands.register(this, DelWarpCommand.class);
        Commands.register(this, DropCommand.class);
        Commands.register(this, ChatCommand.class);
        Commands.register(this, FlyCommand.class);
        Commands.register(this, KickCommand.class);
        Commands.register(this, VanishCommand.class);
        Commands.register(this, BuyExpCommand.class);
        Commands.register(this, SellExpCommand.class);
        Commands.register(this, WorkbenchCommand.class);
        Commands.register(this, EnchantCommand.class);
        Commands.register(this, HealCommand.class);
        Commands.register(this, HelpopCommand.class);
        Commands.register(this, NightVisionCommand.class);

        Commands.register(this, TestCommand.class);

        //Tablist
        tablistHandler = new TabListHandler();

        //Economy
        new EconomyAPI(this);
        SLAPI.loadBalances();
        Commands.register(this, MoneyCommand.class);
        Commands.register(this, PayCommand.class);
        Commands.register(this, EcoCommand.class);
        new EconomySaveScheduler(this);

        //Register custom crafting recipes
        new NetherStarCrafting();
        new TreasureChestCrafting();

        //Register schedulers
        new TablistScheduler(this);

        //CorePlayer things
        CorePlayer.load(this);

        //Random things
        random = new Random();

        //Drop things
        durabilityFixAppliedItems.add(Material.WOOD_SWORD);
        durabilityFixAppliedItems.add(Material.WOOD_PICKAXE);
        durabilityFixAppliedItems.add(Material.WOOD_AXE);
        durabilityFixAppliedItems.add(Material.WOOD_SPADE);
        durabilityFixAppliedItems.add(Material.STONE_SWORD);
        durabilityFixAppliedItems.add(Material.STONE_PICKAXE);
        durabilityFixAppliedItems.add(Material.STONE_AXE);
        durabilityFixAppliedItems.add(Material.STONE_SPADE);
        durabilityFixAppliedItems.add(Material.IRON_SWORD);
        durabilityFixAppliedItems.add(Material.IRON_PICKAXE);
        durabilityFixAppliedItems.add(Material.IRON_AXE);
        durabilityFixAppliedItems.add(Material.IRON_SPADE);
        durabilityFixAppliedItems.add(Material.GOLD_SWORD);
        durabilityFixAppliedItems.add(Material.GOLD_PICKAXE);
        durabilityFixAppliedItems.add(Material.GOLD_AXE);
        durabilityFixAppliedItems.add(Material.GOLD_SPADE);
        durabilityFixAppliedItems.add(Material.DIAMOND_SWORD);
        durabilityFixAppliedItems.add(Material.DIAMOND_PICKAXE);
        durabilityFixAppliedItems.add(Material.DIAMOND_AXE);
        durabilityFixAppliedItems.add(Material.DIAMOND_SPADE);
        durabilityFixAppliedItems.add(Material.SHEARS);
        durabilityFixSwords.add(Material.WOOD_SWORD);
        durabilityFixSwords.add(Material.STONE_SWORD);
        durabilityFixSwords.add(Material.IRON_SWORD);
        durabilityFixSwords.add(Material.GOLD_SWORD);
        durabilityFixSwords.add(Material.DIAMOND_SWORD);
    }

    public void onDisable() {
        ConfigManager.saveAll();
        SLAPI.saveBalances();
        CorePlayer.save(this);
    }

    public static Plugin getPlugin() {
        return plugin;
    }

    public static TabListHandler getTabListHandler() {
        return tablistHandler;
    }

    public static String getTag() {
        return ChatColor.GREEN + getUncolouredTag() + ChatColor.RESET;
    }

    public static String getUncolouredTag() {
        return "[ToCraft.pl] ";
    }

    public static Location getSpawnLocation() {
        ConfigManager.RConfig spawnCfg = ConfigManager.getConfig("spawn");
        if ((spawnCfg != null ? spawnCfg.getString("world") : null) == null)
            return null;
        Location spawn = new Location(null, 0, 0, 0);
        spawn.setX(spawnCfg.getDouble("x"));
        spawn.setY(spawnCfg.getDouble("y"));
        spawn.setZ(spawnCfg.getDouble("z"));
        spawn.setWorld(Bukkit.getWorld(spawnCfg.getString("world")));
        spawn.setYaw(spawnCfg.getInt("yaw"));
        spawn.setPitch(spawnCfg.getInt("pitch"));
        return spawn;
    }

    public static void setSpawnLocation(String world, double x, double y, double z, float yaw, float pitch) throws NullPointerException {
        ConfigManager.RConfig spawnCfg = ConfigManager.getConfig("spawn");
        assert spawnCfg != null;
        spawnCfg.set("world", world);
        spawnCfg.set("x", x);
        spawnCfg.set("y", y);
        spawnCfg.set("z", z);
        spawnCfg.set("yaw", yaw);
        spawnCfg.set("pitch", pitch);
        try {
            spawnCfg.save();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static int randomAmount(int minAmount, int maxAmount) {
        return (int) Math.round(Math.random() * (maxAmount - minAmount) + minAmount);
    }

    public static void givePlayerDrop(Collection<ItemStack> items, Player player) {
        if (items != null) {
            ItemStack[] itemBoard = new ItemStack[items.size()];
            for (int i = 0; i < items.size(); i++) {
                itemBoard[i] = ((ArrayList<ItemStack>) items).get(i);
            }
            player.getInventory().addItem(itemBoard);
        }
    }

    public static void recalculateDurability(Player player) {
        ItemStack item = player.getItemInHand();
        int enchantLevel = item.getEnchantmentLevel(Enchantment.DURABILITY);
        int random = randomAmount(0, 100);
        if ((plugin.durabilityFixAppliedItems.contains(item.getType())) && ((enchantLevel == 0) || (random <= 100 / (enchantLevel + 1)))) {
            if (plugin.durabilityFixSwords.contains(item.getType())) {
                if (item.getDurability() + 2 >= item.getType().getMaxDurability()) {
                    player.getInventory().clear(player.getInventory().getHeldItemSlot());
                    player.playSound(player.getLocation(), Sound.ENTITY_ITEM_BREAK, 1.0F, 1.0F);
                } else {
                    item.setDurability((short) (item.getDurability() + 2));
                }
            } else if (item.getDurability() + 1 >= item.getType().getMaxDurability()) {
                player.getInventory().clear(player.getInventory().getHeldItemSlot());
                player.playSound(player.getLocation(), Sound.ENTITY_ITEM_BREAK, 1.0F, 1.0F);
            } else {
                item.setDurability((short) (item.getDurability() + 1));
            }
            player.updateInventory();
        }
    }

    public static ItemStack fixLeavesDrop(ItemStack drop) {
        // Stare drzewa XD (te sprzed 1.7 czy tam 1.8)
        if (drop.getType().equals(Material.LEAVES)) {
            // Oak leaves
            if (drop.getDurability() == 4 || drop.getDurability() == 8 || drop.getDurability() == 12 || drop.getDurability() == 16 || drop.getDurability() == 20 || drop.getDurability() == 24) {
                return new ItemStack(Material.LEAVES, 1, (short) 0);
            }

            // Spruce leaves
            if (drop.getDurability() == 5 || drop.getDurability() == 9 || drop.getDurability() == 13 || drop.getDurability() == 17 || drop.getDurability() == 21 || drop.getDurability() == 25) {
                return new ItemStack(Material.LEAVES, 1, (short) 1);
            }

            // Birch leaves
            if (drop.getDurability() == 6 || drop.getDurability() == 10 || drop.getDurability() == 18 || drop.getDurability() == 14 || drop.getDurability() == 22 || drop.getDurability() == 26) {
                return new ItemStack(Material.LEAVES, 1, (short) 2);
            }

            // Jungle leaves
            if (drop.getDurability() == 7 || drop.getDurability() == 11 || drop.getDurability() == 15 || drop.getDurability() == 19 || drop.getDurability() == 23 || drop.getDurability() == 27) {
                return new ItemStack(Material.LEAVES, 1, (short) 3);
            }
        }
        // Te gowna co mojang dodal w jakis tam wersjach...
        if (drop.getType().equals(Material.LEAVES_2)) {
            // Akacja
            if (drop.getDurability() == 4 || drop.getDurability() == 8 || drop.getDurability() == 12 || drop.getDurability() == 16) {
                return new ItemStack(Material.LEAVES_2, 1, (short) 0);
            }
            // Dark oak
            if (drop.getDurability() == 5 || drop.getDurability() == 9 || drop.getDurability() == 13 || drop.getDurability() == 17) {
                return new ItemStack(Material.LEAVES_2, 1, (short) 1);
            }

        }

        //If shit, return shit :>
        return new ItemStack(Material.STONE);
    }

    @SuppressWarnings("ConstantConditions")
    public static double getDropChance(String item) {
        return ConfigManager.getConfig("drop").getDouble(item);
    }

    @SuppressWarnings("ConstantConditions")
    public static boolean isWarpExits(String warp) throws NullPointerException {
        return ConfigManager.getConfig("warps").contains(warp.toLowerCase());
    }

    public static void teleportToWarp(Player player, String warp) {
        if (isWarpExits(warp)) {
            if (player.isOnline()) {
                player.teleport(plugin.getWarpLoc(warp));
                sendPrefixed(player, "Zostales przeteleportowany do &6" + warp + "&r.");
            }
        } else {
            sendPrefixed(player, "Warp o tej nazwie nie istnieje.");
        }
    }

    @SuppressWarnings("ConstantConditions")
    public static void createWarp(Double x, Double y, Double z, float yaw, String world, float pitch, String warp, CommandSender sender) throws NullPointerException {
        ConfigManager.getConfig("warps").set(warp + ".x", x);
        ConfigManager.getConfig("warps").set(warp + ".y", y);
        ConfigManager.getConfig("warps").set(warp + ".z", z);
        ConfigManager.getConfig("warps").set(warp + ".yaw", yaw);
        ConfigManager.getConfig("warps").set(warp + ".world", world);
        ConfigManager.getConfig("warps").set(warp + ".pitch", pitch);
        sendPrefixed(sender, "Warp zostal utworzony.");
        ConfigManager.saveAll();
    }

    @SuppressWarnings("ConstantConditions")
    public static void delWarp(String warp, CommandSender sender) {
        if (!isWarpExits(warp)) {
            sendPrefixed(sender, "Warp o podanej nazwie nie istnieje.");
            return;
        }
        ConfigManager.getConfig("warps").set(warp, null);
        sendPrefixed(sender, "Warp zostal usuniety.");
        ConfigManager.save("warps");
    }

    @SuppressWarnings("ConstantConditions")
    public static void listWarps(CommandSender sender) {
        Set<String> warps = ConfigManager.getConfig("warps").getKeys(false);
        String list = "";
        for (String warp : warps) {
            list += "&7" + warp + "&c,";
        }
        if (list.isEmpty()) {
            sendPrefixed(sender, "Nie zostaly utworzone zadne warpy.");
        } else {
            sendPrefixed(sender, "Warpy (" + warps.size() + ") :");
            sendPrefixed(sender, list);
        }

    }

    public static boolean isChatLocked() {
        return plugin.chatLocked;
    }

    public static void toogleChat() {
        if (plugin.chatLocked) {
            broadcastPrefixed("Czat zostal odblokowany.");
            plugin.chatLocked = false;
        } else {
            broadcastPrefixed("Czat zostal zablokowany.");
            plugin.chatLocked = true;
        }
    }

    public static McPlayer getMcPlayer(Player player) {
        if (player == null)
            return null;

        if (playersCache.containsKey(player))
            return playersCache.get(player);

        McPlayer mcPlayer = new McPlayer(player);

        playersCache.put(player, mcPlayer);

        return mcPlayer;
    }

    public static void playerForEach(final Consumer<McPlayer> consumer) {
        playersCache.values().stream()
                .forEach(consumer);
    }

    public static void playerForEachFiltered(final Predicate<McPlayer> predicate, final Consumer<McPlayer> consumer) {
        playersCache.values().stream()
                .filter(predicate)
                .forEach(consumer);
    }

    public static McPlayer getPlayerForCommand(String name) throws CommandException {
        McPlayer mcPlayer;

        if ((mcPlayer = getMcPlayer(Bukkit.getPlayer(name))) == null)
            throw new CommandException(getTag() + ChatColor.RED + "Nie znaleziono gracza: " + ChatColor.AQUA + name);

        return mcPlayer;
    }

    public static void sendPrefixed(CommandSender sender, String message) {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', CorePlugin.getTag() + message));
    }

    public static void send(CommandSender sender, String message) {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
    }

    public static void sendHeader(CommandSender sender, String message){
        send(sender, "&8&m----------[&r &2&l" + message + " &8&m]----------");
    }

    public static void broadcastPrefixed(String message){
        Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', CorePlugin.getTag() + message));
    }

    private void registerEconomy() {
        final ServicesManager sm = getServer().getServicesManager();
        sm.register(Economy.class, new VaultConnector(), this, ServicePriority.Highest);
    }

    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
        if (permissionProvider != null) {
            permission = permissionProvider.getProvider();
        }
        return (permission != null);
    }

    private boolean setupChat() {
        RegisteredServiceProvider<Chat> chatProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.chat.Chat.class);
        if (chatProvider != null) {
            chat = chatProvider.getProvider();
        }

        return (chat != null);
    }

    private boolean setupEconomy() {
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }

        return (economy != null);
    }

    @SuppressWarnings("ConstantConditions")
    public Location getWarpLoc(String warp) throws NullPointerException {
        try {
            if (!isWarpExits(warp))
                return null;
            Location loc = new Location(null, 0, 0, 0);
            loc.setX(ConfigManager.getConfig("warps").getDouble(warp + ".x"));
            loc.setY(ConfigManager.getConfig("warps").getDouble(warp + ".y"));
            loc.setZ(ConfigManager.getConfig("warps").getDouble(warp + ".z"));
            loc.setYaw(ConfigManager.getConfig("warps").getInt(warp + ".yaw"));
            loc.setWorld(Bukkit.getWorld(ConfigManager.getConfig("warps").getString(warp + ".world")));
            loc.setPitch(ConfigManager.getConfig("warps").getInt(warp + ".pitch"));
            return loc;
        } catch (NullPointerException e) {
            return null;
        }
    }
}

