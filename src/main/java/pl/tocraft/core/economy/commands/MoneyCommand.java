package pl.tocraft.core.economy.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.tocraft.core.CorePlugin;
import pl.tocraft.core.economy.EconomyAPI;

public class MoneyCommand extends Command {

    public MoneyCommand() {
        super(new String[]{"money", "pieniadze", "bal", "balance"});
        super.setDescription("Stan konta");
    }

    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        if (args.length == 0) {
            CorePlugin.sendPrefixed(sender, "Stan konta: &6" + EconomyAPI.getBalance(((Player) sender).getUniqueId()) + "&cTC&f.");
        } else {
            try {
                if (EconomyAPI.hasAccount(Bukkit.getOfflinePlayer(args[0]).getUniqueId())) {
                    CorePlugin.sendPrefixed(sender, "Stan konta gracza " + args[0] + ": &6" + EconomyAPI.getBalance(Bukkit.getOfflinePlayer(args[0]).getUniqueId()) + "&cTC&f.");
                } else {
                    CorePlugin.sendPrefixed(sender, "Nieznany gracz.");
                }
            } catch (Exception e) {
                CorePlugin.sendPrefixed(sender, "Wystapil niespodziewany blad podczas pobierania stanu konta O.o");
            }
        }
    }
}
