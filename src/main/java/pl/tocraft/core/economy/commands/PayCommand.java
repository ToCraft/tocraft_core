package pl.tocraft.core.economy.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.themolka.cmds.command.UsageException;
import pl.tocraft.core.CorePlugin;
import pl.tocraft.core.economy.EconomyAPI;

public class PayCommand extends Command {

    public PayCommand() {
        super(new String[]{"pay"});
        super.setUsage("[nick] [ilosc pieniedzy]");
        super.setDescription("Zrob przelew");
    }

    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        if (args.length != 2) {
            throw new UsageException(null);
        } else {
            Player moneySender = (Player) sender;
            Player moneyReciver = Bukkit.getPlayer(args[0]);
            if (!moneyReciver.isOnline()) {
                CorePlugin.sendPrefixed(sender, "Ten gracz jest offline.");
                return;
            }
            String money = args[1];
            if (!EconomyAPI.hasAccount(moneyReciver.getUniqueId())) {
                CorePlugin.sendPrefixed(sender, "Ten gracz nie istnieje.");
            }
            else if (EconomyAPI.getBalance(moneySender.getUniqueId()) < Double.parseDouble(money)) {
                CorePlugin.sendPrefixed(sender, "Nie posiadasz tylu pieniedzy.");
            }else {
                CorePlugin.sendPrefixed(sender, "Przelales &6" + money + "&cTC &fgraczowi &6" + moneyReciver.getName());
                EconomyAPI.setBalance(moneySender.getUniqueId(), EconomyAPI.getBalance(moneySender.getUniqueId()) - Double.parseDouble(money));
                EconomyAPI.setBalance(moneyReciver.getUniqueId(), EconomyAPI.getBalance(moneyReciver.getUniqueId()) + Double.parseDouble(money));
            }
        }
    }

}
