package pl.tocraft.core.economy.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import pl.themolka.cmds.command.Command;
import pl.themolka.cmds.command.CommandException;
import pl.tocraft.core.CorePlugin;
import pl.tocraft.core.economy.EconomyAPI;

import java.util.UUID;

public class EcoCommand extends Command {

    public EcoCommand() {
        super(new String[]{"eco"});
        super.setDescription("Zarzadzanie ekonomia.");
        super.setPermission("tocraft.core.eco");
    }

    public void handle(CommandSender sender, String label, String[] args) throws CommandException {
        if (args.length == 0) {
            CorePlugin.sendPrefixed(sender, "/eco [give/take/set] [nick] [ilosc]");
        } else if (args.length == 3) {
            if (args[0].equalsIgnoreCase("give")) {
                if (args[1] != null) {
                    if (args[2] != null) {
                        UUID uuid = Bukkit.getOfflinePlayer(args[1]).getUniqueId();
                        if (!EconomyAPI.hasAccount(uuid)) {
                            CorePlugin.sendPrefixed(sender, "Gracz nie istnieje.");
                            return;
                        }
                        EconomyAPI.setBalance(uuid, EconomyAPI.getBalance(uuid) + Double.parseDouble(args[2]));
                        CorePlugin.sendPrefixed(sender, "Dodales " + Double.parseDouble(args[2]) + "TC do stanu konta gracza " + args[1]);
                    }
                }
            } else if (args[0].equalsIgnoreCase("take")) {
                if (args[1] != null) {
                    if (args[2] != null) {
                        UUID uuid = Bukkit.getOfflinePlayer(args[1]).getUniqueId();
                        if (!EconomyAPI.hasAccount(uuid)) {
                            CorePlugin.sendPrefixed(sender, "Gracz nie istnieje.");
                            return;
                        }
                        EconomyAPI.setBalance(uuid, EconomyAPI.getBalance(uuid) - Double.parseDouble(args[2]));
                        CorePlugin.sendPrefixed(sender, "Zabrales " + Double.parseDouble(args[2]) + "TC z stanu konta gracza " + args[1]);
                    }
                }
            } else if (args[0].equalsIgnoreCase("set")) {
                if (args[1] != null) {
                    if (args[2] != null) {
                        UUID uuid = Bukkit.getOfflinePlayer(args[1]).getUniqueId();
                        if (!EconomyAPI.hasAccount(uuid)) {
                            CorePlugin.sendPrefixed(sender, "Gracz nie istnieje.");
                            return;
                        }
                        EconomyAPI.setBalance(uuid, Double.parseDouble(args[2]));
                        CorePlugin.sendPrefixed(sender, "Ustawiles stan konta gracza " + args[1] + "na " + Double.parseDouble(args[2]) + "TC");
                    }
                }
            }
        }
    }
}