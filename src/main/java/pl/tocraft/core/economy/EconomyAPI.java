package pl.tocraft.core.economy;

import pl.tocraft.core.CorePlugin;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.UUID;

public class EconomyAPI {

    public static HashMap<UUID, Double> bal = new HashMap<>();
    static DecimalFormat df = new DecimalFormat("#.##");

    /* Api */
    private static CorePlugin plugin;

    public EconomyAPI(CorePlugin instance) {
        plugin = instance;
    }

    public static void setBalance(UUID uuid, double amount) {
        bal.put(uuid, Double.valueOf(df.format(amount)));
    }

    public static Double getBalance(UUID uuid) {
        return Double.valueOf(df.format(bal.get(uuid)));
    }

    public static boolean hasAccount(UUID uuid) {
        return bal.containsKey(uuid);
    }

    public static HashMap<UUID, Double> getBalanceMap() {
        return bal;
    }
}
