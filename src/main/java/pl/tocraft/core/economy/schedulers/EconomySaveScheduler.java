package pl.tocraft.core.economy.schedulers;

import org.bukkit.Bukkit;
import pl.tocraft.core.CorePlugin;
import pl.tocraft.core.economy.SLAPI;

/**
 * Created by artur on 12.08.15.
 */

public class EconomySaveScheduler {
    CorePlugin plugin;

    public EconomySaveScheduler(CorePlugin plugin) {
        this.plugin = plugin;
        Bukkit.getScheduler().runTaskTimer(plugin, SLAPI::saveBalances, 20L, 600L);
    }
}
