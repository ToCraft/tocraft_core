package pl.tocraft.core.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import pl.tocraft.core.CorePlugin;
import pl.tocraft.core.McPlayer;
import pl.tocraft.core.economy.EconomyAPI;

public class PlayerJoinListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        //Player
        Player player = event.getPlayer();

        //Join message
        event.setJoinMessage(null);

        CorePlugin.getMcPlayer(event.getPlayer()); //Add it to cache

        if (!EconomyAPI.hasAccount(event.getPlayer().getUniqueId()))
            EconomyAPI.setBalance(event.getPlayer().getUniqueId(), 0);

        if (!event.getPlayer().hasPermission("tocraft.core.vanish")) {
            //Vanish
            CorePlugin.playerForEachFiltered( //Hide vanished players
                    McPlayer::isVanished,
                    p -> player.hidePlayer(p.getPlayer())
            );
        }
    }

}
