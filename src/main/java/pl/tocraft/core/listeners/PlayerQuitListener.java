package pl.tocraft.core.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffectType;
import pl.tocraft.core.CorePlugin;
import pl.tocraft.core.McPlayer;

public class PlayerQuitListener implements Listener {

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        McPlayer mcPlayer = CorePlugin.getMcPlayer(event.getPlayer());

        // Remove quit message
        event.setQuitMessage(null);

        // Remove vanish
        if (mcPlayer.isVanished()) {
            mcPlayer.setVanished(false);
            event.getPlayer().removePotionEffect(PotionEffectType.INVISIBILITY);
        }
    }
}
