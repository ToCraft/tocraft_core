package pl.tocraft.core.listeners;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import pl.tocraft.core.CorePlayer;
import pl.tocraft.core.CorePlugin;

import java.util.Collection;

/*
Dzialanie systemu dropu:
POdzial dropu na "tiery"
Tier to taki jakby nasz poziom, od "tieru" zalezy nasz drop.

Dziękuje @XopyIP za pomysł:
XopyIP przed chwilą - XopyIP: np tier 1 - wegiel + iron
XopyIP przed chwilą - XopyIP: tier 2 gold + cos tam
XopyIP przed chwilą - XopyIP: i tak dalej :D
XopyIP przed chwilą - XopyIP: aby miec diaxa trzeba miec np tier 4 lub 5
XopyIP przed chwilą - XopyIP: wtedy aby bylo nas stac na jakies ity trzeba pograc troche a nie liczyc na szczescie :d
XopyIP przed chwilą - XopyIP: artur mozesz progressbara dodac :D
XopyIP przed chwilą - XopyIP: w tabie najlepiej
XopyIP przed chwilą - XopyIP: tymi pelnymi znaczkami
XopyIP przed chwilą - XopyIP: pokolorowanymi
XopyIP przed chwilą - XopyIP: zielone te co juz masz i czerwone ktorych brakuuje :d
XopyIP przed chwilą - XopyIP: i tak 4 kolumny xd
XopyIP przed chwilą - XopyIP: lub 2 w srodku co sa

Podzial:
Tier 1 - wegiel, zelazo
Tier 2 - zloto, lapis
Tier 3 - redstone
Tier 4 - emerald
Tier 5 - diamond, spawner

 */

public class DropListener implements Listener{
    @EventHandler(priority=EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event){
        if(event.isCancelled()){
            return;
        }
        if(event.getPlayer().getGameMode().equals(GameMode.CREATIVE)){
            return;
        }
        if(event.getBlock().getType().equals(Material.WHEAT) ||  event.getBlock().getType().equals(Material.SEEDS) || event.getBlock().getType().equals(Material.NETHER_WARTS) || event.getBlock().getType().equals(Material.MELON_STEM) || event.getBlock().getType().equals(Material.PUMPKIN_STEM) || event.getBlock().getType().equals(Material.CARROT) || event.getBlock().getType().equals(Material.CROPS) || event.getBlock().getType().equals(Material.POTATO)){
            return;
        }

        Player player = event.getPlayer();
        CorePlayer coreplayer = CorePlayer.getCorePlayer(player.getUniqueId());
        Block block = event.getBlock();

        Collection<ItemStack> drops = block.getDrops(player.getItemInHand());

        if(block.getType().equals(Material.SNOW)){
            drops.add(new ItemStack(Material.SNOW_BALL));
        }

        if (block.getType().equals(Material.LEAVES) || block.getType().equals(Material.LEAVES_2)) {
            if (event.getPlayer().getItemInHand().getType() == Material.SHEARS) {
                drops.add(CorePlugin.fixLeavesDrop(new ItemStack(block.getType(), 1, (short) block.getData())));
            }
        }

        // drop z duzej trawy
        if(block.getType().equals(Material.LONG_GRASS)){
            // nasiona dynii
            if(Math.random() * 100.0D <= 0.5){
                drops.add(new ItemStack(Material.PUMPKIN_SEEDS, 1));
            }
            // nasiona arbuza
            if(Math.random() * 100.0D <= 0.75){
                drops.add(new ItemStack(Material.MELON_SEEDS, 1));
            }
        }

        // drop z stone
        if(block.getType().equals(Material.STONE) || block.getType().equals(Material.COBBLESTONE)){
            coreplayer.addDropExp(CorePlugin.randomAmount(0, 5));
            player.giveExp(CorePlugin.randomAmount(0, 3));
            CorePlugin.economy.depositPlayer(player, 0.01);
            /* Wszystkie tiery */
            if(Math.random() * 100.0D <= CorePlugin.getDropChance("coal")){
                double fortuneRandom1 = Math.random() * 100.0D;
                double fortuneRandom2 = Math.random() * 100.0D;
                double fortuneRandom3 = Math.random() * 100.0D;
                ItemStack wegiel = new ItemStack(Material.COAL, 1);
                if ((30.0D <= fortuneRandom1) && (player.getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) >= 1)) {
                    wegiel.setAmount(2);
                } else if ((20.0D <= fortuneRandom2) && (player.getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) >= 2)) {
                    wegiel.setAmount(3);
                } else if ((10.0D <= fortuneRandom3) && (player.getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) >= 3)) {
                    wegiel.setAmount(4);
                }
                drops.add(wegiel);
                CorePlugin.sendPrefixed(player, "Znalazles " + wegiel.getAmount() + " wegiel!");
                coreplayer.addDropExp(3 * wegiel.getAmount());
            }
            if(Math.random() * 100.0D <= CorePlugin.getDropChance("iron")){
                drops.add(new ItemStack(Material.IRON_ORE));
                CorePlugin.sendPrefixed(player, "Znalazles zelazo!");
                coreplayer.addDropExp(15);
            }
            /* Od tieru 2 */
            if(coreplayer.getDropTier() >= 2){
                if(Math.random() * 100.0D <= CorePlugin.getDropChance("gold")){
                    drops.add(new ItemStack(Material.GOLD_ORE));
                    CorePlugin.sendPrefixed(player, "Znalazles zloto!");
                    coreplayer.addDropExp(21);
                }
                if(Math.random() * 100.0D <= CorePlugin.getDropChance("lapis")){
                    int amount = CorePlugin.randomAmount(1, 6);
                    drops.add(new ItemStack(Material.INK_SACK, amount, (short) 4));
                    CorePlugin.sendPrefixed(player, "Znalazles " + amount + " lapis!");
                    coreplayer.addDropExp(5 * amount);
                }
            }
            /* Od tieru 3 */
            if(coreplayer.getDropTier() >= 3){
                if(Math.random() * 100.0D <= CorePlugin.getDropChance("redstone")){
                    int amount = CorePlugin.randomAmount(1, 8);
                    drops.add(new ItemStack(Material.REDSTONE, amount));
                    CorePlugin.sendPrefixed(player, "Znalazles " + amount + " redstone!");
                    coreplayer.addDropExp(7 * amount);
                }
            }
            /* Od tieru 4 */
            if(coreplayer.getDropTier() >= 4){
                if(Math.random() * 100.0D <= CorePlugin.getDropChance("emerald")){
                    double fortuneRandom1 = Math.random() * 100.0D;
                    double fortuneRandom2 = Math.random() * 100.0D;
                    double fortuneRandom3 = Math.random() * 100.0D;
                    ItemStack emerald = new ItemStack(Material.EMERALD, 1);
                    if ((30.0D <= fortuneRandom1) && (player.getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) >= 1)) {
                        emerald.setAmount(CorePlugin.randomAmount(2, 3));
                    } else if ((20.0D <= fortuneRandom2) && (player.getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) >= 2)) {
                        emerald.setAmount(CorePlugin.randomAmount(2, 4));
                    } else if ((10.0D <= fortuneRandom3) && (player.getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) >= 3)) {
                        emerald.setAmount(CorePlugin.randomAmount(2, 5));
                    }
                    drops.add(emerald);
                    CorePlugin.sendPrefixed(player, "Znalazles " + emerald.getAmount() + " szmaragd!");
                    coreplayer.addDropExp(12 * emerald.getAmount());
                }
            }
            /* Tier 5 - ostatni */
            if(coreplayer.getDropTier() >= 5){
                if(Math.random() * 100.0D <= CorePlugin.getDropChance("diamond")){
                    double fortuneRandom1 = Math.random() * 100.0D;
                    double fortuneRandom2 = Math.random() * 100.0D;
                    double fortuneRandom3 = Math.random() * 100.0D;
                    ItemStack diamond = new ItemStack(Material.DIAMOND, 1);
                    if ((30.0D <= fortuneRandom1) && (player.getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) >= 1)) {
                        diamond.setAmount(CorePlugin.randomAmount(2, 3));
                    } else if ((20.0D <= fortuneRandom2) && (player.getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) >= 2)) {
                        diamond.setAmount(CorePlugin.randomAmount(2, 5));
                    } else if ((10.0D <= fortuneRandom3) && (player.getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) >= 3)) {
                        diamond.setAmount(CorePlugin.randomAmount(2, 6));
                    }
                    drops.add(diamond);
                    CorePlugin.sendPrefixed(player, "Znalazles " + diamond.getAmount() + " diament!");
                    coreplayer.addDropExp(19 * diamond.getAmount());
                }

                // drop spawnerow ;>
                if(Math.random() * 100.0D <= 0.000000001){
                    coreplayer.addDropExp(CorePlugin.randomAmount(500, 1200));
                    player.giveExp(CorePlugin.randomAmount(7500, 15000));
                    CorePlugin.broadcastPrefixed("&6Gratulacje! &fGracz " + player.getName() + " znalazl spawner!");
                    drops.add(new ItemStack(Material.MOB_SPAWNER));
                }
            }

            if(coreplayer.getDropTier() != 5){
                // Meh, badziewny kod na sprawdzanie czy zlevelupowal xD
                if(coreplayer.tierLevelUp()){
                    CorePlugin.sendPrefixed(player, "Osiagnales nastepny tier.");
                }
            }

        }

        CorePlugin.givePlayerDrop(drops, player);
        CorePlugin.recalculateDurability(player);
        block.setType(Material.AIR);
    }
}
