package pl.tocraft.core.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import pl.tocraft.core.CorePlugin;
import tk.northpl.tab.TablistSlot;

import java.util.concurrent.CopyOnWriteArrayList;

@SuppressWarnings("unused")
public class TablistPlayerLeaveListener implements Listener {
    @EventHandler
    public void onLeave(final PlayerQuitEvent e) {
        this.cleanup(e.getPlayer().getName());
    }

    @EventHandler
    public void onLeave(final PlayerKickEvent e) {
        this.cleanup(e.getPlayer().getName());
    }

    private void cleanup(final String player) {
        for (final TablistSlot tablistSlot : CorePlugin.getTabListHandler().getSlots().values())
            new CopyOnWriteArrayList<>(tablistSlot.getCustomPlayersOptions()).stream().filter(cpt -> cpt.getPlayerNick().equals(player)).forEach(cpt -> tablistSlot.getCustomPlayersOptions().remove(cpt));
    }
}

