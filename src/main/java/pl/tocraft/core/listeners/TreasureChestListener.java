package pl.tocraft.core.listeners;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import pl.tocraft.core.CorePlugin;

import java.util.ArrayList;
import java.util.Random;

@SuppressWarnings({"unused", "SpellCheckingInspection"})
public class TreasureChestListener implements Listener {

    private static ArrayList<ItemStack> items = new ArrayList<>();

    static{
        // Clear items list.
        items.clear();

        // Add items to items list.
        items.add(new ItemStack(Material.DIAMOND, 16));
        items.add(new ItemStack(Material.EMERALD, 16));
        items.add(new ItemStack(Material.DIRT, 4));
        items.add(new ItemStack(Material.LAVA_BUCKET, 1));
        items.add(new ItemStack(Material.CACTUS, 32));
        items.add(new ItemStack(Material.CARROT, 32));
        items.add(new ItemStack(Material.WEB, 32));
        items.add(new ItemStack(Material.MELON, 20));
        items.add(new ItemStack(Material.DIAMOND_BOOTS, 1));
        items.add(new ItemStack(Material.IRON_AXE, 1));
        items.add(new ItemStack(Material.DIAMOND_PICKAXE, 1));
        items.add(new ItemStack(Material.DIAMOND_LEGGINGS, 1));
        items.add(new ItemStack(Material.DIAMOND_CHESTPLATE, 1));
        items.add(new ItemStack(Material.REDSTONE, 64));
        items.add(new ItemStack(Material.BOOK, 16));
        items.add(new ItemStack(Material.SUGAR_CANE, 8));
    }

    @EventHandler
    public void onRightClick(PlayerInteractEvent event){
        //Kod do dópy!

        //Przerwie metode gdy akcja nie bedzie ppm na air lub na blok
        if(event.getAction() != Action.RIGHT_CLICK_AIR && event.getAction() != Action.RIGHT_CLICK_BLOCK)
            return;

        Player player = event.getPlayer();
        if(event.getPlayer().getItemInHand().getType() == Material.CHEST){
            if(event.getPlayer().getItemInHand().hasItemMeta()){
                ItemMeta meta = event.getPlayer().getItemInHand().getItemMeta();
                if(meta.getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "Skrzynia Skarbow")){
                    event.setCancelled(true);

                    if((player.getItemInHand().getAmount() - 1) == 0){
                        player.getInventory().removeItem(player.getItemInHand());
                    }else{
                        player.getInventory().getItem(player.getInventory().getHeldItemSlot()).setAmount(player.getItemInHand().getAmount() - 1);
                    }
                    player.updateInventory();
                    ItemStack reward = items.get(new Random().nextInt(items.size()));
                    player.getInventory().addItem(reward);
                    CorePlugin.sendPrefixed(player, "Otworzyles skrzynie skarbow! Otrzymales &6" + reward.getAmount() + " " + reward.getType().toString().toLowerCase().replace('_', ' '));
                    player.updateInventory();
                }
            }
        }
    }
}
