package pl.tocraft.core.listeners;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers;
import com.comphenix.protocol.wrappers.PlayerInfoData;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import pl.tocraft.core.CorePlugin;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class TablistPlayerJoinListener implements Listener {
    @EventHandler
    public void onJoin(final PlayerJoinEvent e) {
        /* Custom tablist */
        final PacketContainer writeTab = CorePlugin.getTabListHandler().getProtocol().createPacket(PacketType.Play.Server.PLAYER_INFO);
        writeTab.getPlayerInfoAction().write(0, EnumWrappers.PlayerInfoAction.ADD_PLAYER);

        final List<PlayerInfoData> virtualPlayersToWrite = new ArrayList<>(80);
        virtualPlayersToWrite.addAll(CorePlugin.getTabListHandler().getSlots().values().stream().map(wgp -> new PlayerInfoData(wgp.getVirtualPlayer(), 100, EnumWrappers.NativeGameMode.NOT_SET, WrappedChatComponent.fromText(wgp.getTextForPlayer(e.getPlayer().getName())))).collect(Collectors.toList()));
        writeTab.getPlayerInfoDataLists().write(0, virtualPlayersToWrite);
        try {
            CorePlugin.getTabListHandler().getProtocol().sendServerPacket(e.getPlayer(), writeTab);
        } catch (final InvocationTargetException ignored) {
        }

        /* Header/footer */
        PacketContainer pc = ProtocolLibrary.getProtocolManager().createPacket(PacketType.Play.Server.PLAYER_LIST_HEADER_FOOTER);

        pc.getChatComponents().write(0, WrappedChatComponent.fromText(ChatColor.BLUE + "ToCraft.pl")) //Header
                .write(1, WrappedChatComponent.fromText("")); //Footer
        try
        {
            ProtocolLibrary.getProtocolManager().sendServerPacket(e.getPlayer(), pc);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
