/*
    This file is part of tocraft_core.

    tocraft_core is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    tocraft_core is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    http://www.gnu.org/licenses/gpl-3.0.en.html
 */
package pl.mrgregorix.jsonchatapi;

public enum ChatSelector {
    RANDOM('r'),
    NEAREST('p'),
    ALL_ENTITIES('e'),
    ALL('a');

    private char c;

    ChatSelector(char c) {
        this.c = c;
    }

    public char getChar() {
        return c;
    }
}
