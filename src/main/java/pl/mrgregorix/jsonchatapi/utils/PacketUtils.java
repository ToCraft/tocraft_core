/*
    This file is part of tocraft_core.

    tocraft_core is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    tocraft_core is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    http://www.gnu.org/licenses/gpl-3.0.en.html
 */
package pl.mrgregorix.jsonchatapi.utils;

import com.google.common.collect.Maps;
import org.apache.commons.lang.Validate;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;

public final class PacketUtils {
    private static boolean particlesEnabled;
    private static HashMap<String, Object> particlesByString = Maps.newHashMap();

    static {
        try {
            Class<?> clazz = ReflectionUtils.getNMSClass("EnumParticle");

            Method method = null;
            for (Method m : clazz.getMethods())
                if ((m.getModifiers() & Modifier.STATIC) == 0 && m.getReturnType() == String.class && m.getName().length() == 1)
                    method = m;

            for (Field field : clazz.getDeclaredFields()) {
                if ((field.getModifiers() & Modifier.STATIC) == 0)
                    continue;
                field.setAccessible(true);
                Object value = field.get(null);

                if (clazz.isInstance(value)) {
                    particlesByString.put((String) method.invoke(value), value);
                }
            }
            particlesEnabled = true;
        } catch (Exception e) {
            particlesEnabled = false;
        }
    }

    private PacketUtils() {
    }

    public static Object createPacketUsingSingleCtor(String packetName, Object... params) {
        try {
            Class packetClass = ReflectionUtils.getNMSClass(packetName);
            Constructor constructor = null;
            for (Constructor ctor : packetClass.getConstructors()) {
                if (ctor.getParameterTypes().length == params.length) {
                    if (constructor != null)
                        return null;
                    constructor = ctor;
                }
            }

            return constructor.newInstance(params);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static Object createPacket(String packetName, Object... params) {
        Validate.isTrue(params.length % 2 == 0, "Bad params");
        Validate.notEmpty(params, "Bad params");

        Object[] parameters = new Object[params.length / 2];
        Class[] parameterTypes = new Class[params.length / 2];

        for (int i = 0; i < params.length; i += 2) {
            parameterTypes[i / 2] = (Class) params[i];
            parameters[i / 2] = params[i + 1];
        }

        try {
            Class packetClass = ReflectionUtils.getNMSClass(packetName);
            Constructor<?> constructor = packetClass.getConstructor(parameterTypes);
            constructor.setAccessible(true);

            return constructor.newInstance(parameters);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void sendPacket(Object packet, Player player) {
        Object entityPlayer = ReflectionUtils.getHandle(player);
        Object playerConnection = ReflectionUtils.getPrivateField(entityPlayer.getClass(), "playerConnection", entityPlayer);
        ReflectionUtils.invokePrivateMethod(playerConnection.getClass(), "sendPacket", playerConnection, new Class[]{ReflectionUtils.getNMSClass("Packet")}, packet);
    }

    public static void createParticleEfect(String particle, Player player, float speed, int amount, float x, float y, float z) {
        if (!particlesEnabled)
            return;

        Object particleObject = particlesByString.get(particle);

        Object packet;

        try {
            packet = PacketUtils.createPacketUsingSingleCtor(
                    "PacketPlayOutWorldParticles",
                    particleObject,
                    false,
                    (float) player.getLocation().getX(),
                    (float) player.getLocation().getY(),
                    (float) player.getLocation().getZ(),
                    x, y, z,
                    speed,
                    amount,
                    new int[0] //USER CAN'T SET METADATA HUE HUE HUE HUE
            );
        } catch (Exception e) {
            System.out.println("Unknown particle effect: " + particle);
            return;
        }

        PacketUtils.sendPacket(packet, player);
    }
}
